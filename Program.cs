﻿// See https://aka.ms/new-console-template for more information

bool start = EnterPressed();
int firstNumber = 0;
int lastNumber = 0;
int arrayCapacity;

while (!start)
{
    start = EnterPressed();
}

while ((firstNumber <= 0) || (firstNumber > 100))
{
    Console.WriteLine("\nВведите первое число последовательности");

    firstNumber = GetNumber();

    if ((firstNumber <= 0) || (firstNumber > 100))

    { Console.WriteLine("\nУпс, согласно заданию, числа от 1 до 100"); }

}

while ((lastNumber <= firstNumber) || (lastNumber > 100))
{
    Console.WriteLine("\nВведите последнее число последовательности");

    lastNumber = GetNumber();

    if ((lastNumber <= firstNumber) || (lastNumber > 100))

    { Console.WriteLine("\nУпс, последнее число не может быть меньше первого и больше 100"); }
}

List<int> array = new List<int>(lastNumber-firstNumber+1);

for (int i =0; i < (array.Capacity); i++)
{
    array.Add(firstNumber + i);
}

Console.WriteLine("\n");

foreach (var a in array)
{
    if (a % 3 != 0 && a % 5 != 0)
    { Console.WriteLine(a.ToString()); }
    else if (a % 3 == 0 && a % 5 != 0)
    { Console.WriteLine("Fizzy"); }
    else if (a % 3 != 0 && a % 5 == 0)
    { Console.WriteLine("Buzzy"); }
    else { Console.WriteLine("FizzyBuzzy"); }
}

Console.WriteLine("\nНа этом все, спасибо за внимание! Нажмите любую кнопку!");
Console.ReadLine();


static int GetNumber()
{
  
    var Number = Console.ReadLine();

    if (int.TryParse(Number, out int Number32))
    {
        return Number32;
    }
    else { 
            Console.WriteLine("\nНекорректный ввод");
            return 0;
         }
}
static bool EnterPressed()
{
    Console.WriteLine("\nНажмите Enter для старта, или Ctrl+C для выхода");

    ConsoleKeyInfo startKey = new ConsoleKeyInfo();

    startKey = Console.ReadKey();

    if (startKey.Key == ConsoleKey.Enter)
    {
        Console.WriteLine("Ok");
        return true;
    }
    else { return false; }

}